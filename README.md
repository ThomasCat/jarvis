# Jarvis

A C++ chatbot for Linux, macOS and Android I wrote during the holidays. Responds to natural English instead of just commands. Inspired by the Jarvis AI from Marvel's Iron Man.

### Some questions you can ask:


> What's the weather like? 

> How are you doing. 

> You got a dictionary built in, buddy?

> Give me a system report, Jarvis.

> Update yourself.

> Is Santa Claus real?

> What's the weather right now?

> What can you do for me?

> Good Morning.

> Set an alarm for me please.

> Do me a favor and destroy everything. (Try this one at your own risk.)

### Instructions

<b>Make sure you have the following installed:</b> [espeak](https://github.com/rhdunn/espeak), [sox](https://github.com/chirlu/sox), GCC, xdg-open, git.

#### Linux and macOS:

<b>Note</b>: Make sure SIP is disabled on macOS/OS X and Git is installed. 

1. Open a terminal window and clone this project by running ```git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/jarvis```
2. Change directory via ```cd jarvis```
3. To start, type ```chmod +x jarvis``` and ```./jarvis```

To permanently *install* Jarvis on your device, tell him to install himself on your device.

To *update* Jarvis, ask him to perform a system update.

To *uninstall* Jarvis from your system, start Jarvis and tell him uninstall himself.

#### Android:
1. Download and install [Termux](https://github.com/termux/termux-app) on your device.
2. Open Termux and install some packages by running these commands:
    ```
    pkg install git
    pkg install clang
    pkg install pulseaudio
    pkg install sox
    ```
    <!--pkg install python-->
3. Run the following command next ```git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/jarvis```
4. To start, type ```chmod +x jarvis``` and ```./jarvis```

<b>Tip</b>: To integrate text-to-speech output, run ```chmod +x jarvis && ./jarvis | espeak -p 55 -s 210 -v english_rp```

### Improvements to be made

Add confidence score, integrate with TensorFlow, porting to Python, using MySQL database or CSV files instead of hardcoded strings, inbuilt TTS and voice recognition.

[Open-source notices and Credits](NOTICE)
<hr>
<b>License</b>: 

<a href="http://www.gnu.org/licenses/gpl-3.0.en.html" rel="nofollow"><img src="https://camo.githubusercontent.com/0e71b2b50532b8f93538000b46c70a78007d0117/68747470733a2f2f7777772e676e752e6f72672f67726170686963732f67706c76332d3132377835312e706e67" alt="GNU GPLv3 Image" data-canonical-src="https://www.gnu.org/graphics/gplv3-127x51.png" width="80"></a><br>[Copyright © 2019  Owais Shaikh](LICENSE)