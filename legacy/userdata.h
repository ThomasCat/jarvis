/*
    Jarvis-Chatbot
    Copyright (C) 2019  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a compareString of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//This file is used to store user data
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cmath>
#ifndef USERDATA
#define MYHEADER
using namespace std;

class userData{
public:
	int isDataSet=0; //Check if data is already set by user
	int talkback=0;
	string firstName="fname"; //set names
	string lastName="lname";
	string gender="sex";
	string ti="title"; //Mr., Ms.
	string toBeCalled="pref"; //Sir, Ma'am by default, can be set to custom value as well
	int alarmHH=6;    //default value 6
	int alarmMM=30;   //default value 30
	int age=19;
	int height=176, weight=60;
	int bmi=weight/(height*height); //Just to make sure you fit in the Iron Man suit ;)

	std::string userPronoun(){
		return toBeCalled;
	}
	
	string reminders(){
		//To set reminders
		typedef struct{ //Reminder struct
		    string date;
		    string time;
		    string text;
		}reminder;
	
		reminder remindersList[]{ //Actual reminders
			{"7/8/2019",
			{"15:24",
			{"First reminder",}}},
		};
	}	

};

#endif