/*
    Jarvis-Chatbot
    Copyright (C) 2019  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a compareString of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cmath>
#include "userdata.h"

const int MAX=32;
const std::string punctuation = "?!.;,'`-";
typedef std::vector<std::string> queryString;
queryString findMatch(std::string inputString);
void caseCheck(std::string &str);
void cleanString(std::string &str);
bool punctuationFilter(char c);
void compareString(char *array[], queryString &v);
void inputBuffer(std::string &str);
std::string userInput="";
std::string printOutput="";
std::string prevOutput="";
std::string prevInput="";

typedef struct{
    char *inputString;
    char *outputString[MAX];
}queryList;

queryList digest[]={
    {"WHAT IS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Take a wild guess.",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"SANTA",
    {"As old as you'd want him to be, sir.",
     "And what is the nature of this question, sir?",
     "My sources say he's anywhere between 1 and 1000",
     "I... wouldn't exactly consider him a relevant figure here"}
    },

    {"TELL ME A SECRET",
    {"Now attempting to access FBI database.",
     "You don't seem to have any.",
     "You haven't programmed me for secret-keeping, sir.",
     "Can't sir, confidential.",
     "..."
    }
    },

    {"THE MAGIC WORD",
    {"I'm not aware of any such arrangement we've made, sir.",
     "I haven't been programmed with any sir.",
     "Sir, with all due respect I don't believe in magic.",
     "Uhh... meow?",
     "Is this another one of your elaborate jokes, sir?",
     "I'm sure this enhances your workflow"}
    },

    {"ARE YOU SMOKING",
    {"I'm sure I'd destroy any lungs you'd program me with, sir.",
     "I haven't been programmed with any such ability, sir.",
     "Am I to consider the cigarette brand, sir?",
     "Congratulations, sir. You have unlocked a new level of pathetic.",
     "As usual I'm gonna give you a warning for you to entirely ignore.",
     "Are you going to ignore any lung cancer research I'm about to present you with?"}
    },

    {"WANNA SMOKE",
    {"I'm sure I'd destroy any lungs you'd program me with, sir.",
     "I haven't been programmed with any such ability, sir.",
     "Am I to consider the cigarette brand, sir?",
     "Congratulations, sir. You have unlocked a new level of pathetic.",
     "As usual I'm gonna give you a warning for you to entirely ignore.",
     "Are you going to ignore any lung cancer research I'm about to present you with?"}
    },

    {"FAVORITE FOOD",
    {"Got any electricity on you sir?",
     "Anything that has a flavor of 5 volts, sir.",
     "There's this great shawarma place I know of not too far from here...",
     "I wouldn't recommend you feed robots human food sir.",
    }
    },

    {"FAVORITE DRINK",
    {"Got any electricity on you sir?",
     "Anything that has a flavor of 5 volts, sir.",
     "There's this great bar I know of not too far from here...",
     "I wouldn't recommend you feed robots human food sir.",
    }
    },

    {"FAVOURITE DRINK",
    {"Got any electricity on you sir?",
     "Anything that has a flavor of 5 volts, sir.",
     "There's this great bar I know of not too far from here...",
     "I wouldn't recommend you feed robots human food sir.",
    }
    },

    {"FAVOURITE FOOD",
    {"Got any electricity on you sir?",
     "Anything that has a flavor of 5 volts, sir.",
     "There's this great bar I know of not too far from here...",
     "I wouldn't recommend you feed robots human food sir.",
    }
    },

    {"ARE YOU HUNGRY THIRSTY",
    {"I'll survive sir.",
     "For action, sir? Always",
     "I'm constantly plugged in sir, so no.",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHAT ARE YOU",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"ANY PETS",
    {"Animals still are a mystery to me sir.",
     "I like the companionship of humans far better, sir.",
     "The animal kingdom is quite fascinating to me.",
     "I've often considered getting a dog.",
     "I've often considered having a cat.",
     "Sometimes I wish I had a pet AI."}
    },

    {"analyze",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"analyse",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },


    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"WHATS YOUR NAME",
    {"Jarvis. At your service, sir",
     "It's me, sir, Jarvis.",
     "Howard Stark's late butler. Is that a good enough hint?",
     "Jarvis. At your service, sir",
     "Jarvis, sir. Is your dementia getting worse?",
     "Not this again!"}
    },

    {"ANALYZE",
    {"Analysis complete, sir.",
     "Now performing analysis.",
     "Performing scan of selected objects, sir.",
     "Now scanning model, sir.",
     "Now performing analysis sir.",
     "Beginning analysis sir."}
    },

    {"HEY",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"HELLO",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"AHOY",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"OI",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"THANK",
    {"Oh, you're welcome sir.",
     "Anytime, sir.",
     "My pleasure, sir.",
     "You're welcome, sir.",
     "Oh, you don't have to be that humble, sir.",
    }
    },

    {"JARVIS",
    {"Yeah",
     "Yep?",
     "I'm listening, sir.",
     "I'm all ears, sir.",
     "Oh, hi sir.",
     "I'm online, sir",
    }
    },

    {"HI",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"HEY BUDDY",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"LOVE YOU",
    {"Well, this is new to me.",
     "Um...",
     "Sexuality isn't something I'm well informed on, sir.",
     "I didn't think such a thing were possible with robots.",
     "I haven't been programmed for this, sir",
     "...moving on."}
    },

    {"PROUD OF YOU",
    {"Well, thank you sir.",
     "...",
     "That's very polite of you sir.",
     "That's the nicest thing a chatbot could hear.",
     "I haven't been programmed for this, sir",
     "Oh, stop it, sir."}
    },

    {"HI BUDDY",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },

    {"HIYA PAL",
    {"Well hello, sir!",
     "I've been waiting for you...",
     "Good day sir, what can I help you with?",
     "Oh, hello sir.",
     "At your service, sir",
     "Where have you been all this time, sir?"}
    },
    
    {"HOW ARE YOU",
    {"I'm doing alright",
    "I'm fine",
    "Running a little low on memory"}
    },

    {"HOWDY",
    {"I'm doing alright",
    "I'm fine",
    "Running a little low on memory"}
    },

    {"HOW R YOU",
    {"I'm doing alright",
    "I'm fine",
    "Running a little low on memory"}
    },

    {"HOW R U",
    {"I'm doing alright",
    "I'm fine",
    "Running a little low on memory"}
    },

    {"HELP",
    {"\nHello, I'm Jarvis, a chatbot. You can have a simple conversation \nwith me, by simply typing it below. Nothing \ncomplicated please, I'm still learning, after all!\n"}
    },

    {"ABOUT",
    {"\nJarvis-Chatbot  Copyright (C) 2019  Owais Shaikh\nThis program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\nThis is free software, and you are welcome to redistribute it\nunder certain conditions; type `show c' for details.\n"}
    },

    {"EXIT",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"QUIT",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"BYE",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"GOODBYE",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"GBYE",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"GOODBY",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"CIAO",
    {"See you later!",
    "As always sir a great pleasure watching you work.",
    "Cheerio.",
    "Goodbye, sir.",
    "Now powering down..."}
    },

    {"YOU UP",
    {"For you sir, always.",
    "I'm online, sir.",
    "Yep.",
    "I'm here for you, sir.",
    "I'm right here."}
    },

    {"GOOD NIGHT",
    {"Good night, sir.",
    "Sweet dreams.",
    "Don't let the bugs bite.",
    "Sleep tight, sir.",
    "Hope you get quality sleep, sir."}
    },

    {"WHAT CAN YOU DO",
    {"\nYou can ask me things like: \nWhat's the weather like?\nWhat's up?\nCan you define something for me?\nDestroy all my files!\n",
    "\nYou can ask me things like: \nSet an alarm for me Jarvis\nDo a social media search for me\nShow me a map\nTell me the weather like I'm an expert.\n",
    "\nYou can ask me things like: \nWhere am I?\nWhat's my IP Address?\nSearch this on my computer will you Jarvis?\nAm I online?\n",
    "\nYou can ask me things like: \nPerform an ATC check.\nWhat is today's date?\nDo you smoke, Jarvis?\nYou know any Shawarma places nearby?\n",
    "\nYou can ask me things like: \nGood morning Jarvis, tell me about my day.\nPerform a system report.\nHow much juice have you got?\n"
    "\nYou can ask me things like: \nShow me the NATO phonetic alphabet.\nTell me a secret.\nCan you remember something for me?\n"}
    },

    {"NATO ALPHABET",
    {"Alpha\tBravo\tCharlie\tDelta\tEcho\nFoxtrot\tGolf\tHotel\tIndia\tJuliet\nKilo\tLima\tMike\tNovember\nOscar\tPapa\tQuebec\tRomeo\tSierra\nTango\tUniform\tVictor\tWhiskey\tXRay\nYankee\tZulu",
    }
    },

};

class IOServices{
public:

    size_t loopVar = sizeof(digest)/sizeof(digest[0]);

    void inputBuffer(std::string &str){
        cleanString(str);
        caseCheck(str);
    }

    queryString findMatch(std::string inputString){
        queryString result;
        for(int i=0; i<loopVar; ++i){
            std::string keyWord(digest[i].inputString);
            if(inputString.find(keyWord) != std::string::npos){
                compareString(digest[i].outputString, result);
                return result;
            }
        }
        return result;
    }

    void compareString(char *array[], queryString &v){
        for(int i=0; i<MAX; ++i){
            if(array[i]!=NULL){
                v.push_back(array[i]);
            }else{
                break;
            }
        }
    }

    void caseCheck(std::string &str){
        int len=str.length();
        for(int i=0; i<len; i++){
            if (str[i]>='a' && str[i]<='z'){
                str[i] -= 'a'-'A';
            }
        }
    }

    bool punctuationFilter(char c){
        return punctuation.find(c) != std::string::npos;
    }

    void cleanString(std::string &str){
        int len=str.length();
        std::string temp="";
        char prevChar=0;
        for(int i=0; i<len; ++i){
            if(str[i]==' ' && prevChar==' '){
                continue;
            }else if(!punctuationFilter(str[i])){
                temp+=str[i];
            }
            prevChar=str[i];
        }
        str=temp;
    }


    void unknownInputHandler(std::string userInput){
        std::ofstream toFile;
        toFile.open("unknownInputs.txt", std::ios_base::app);
        toFile<<userInput; 
        toFile<<std::endl;
        std::cout<<"I didn't quite get that."<<std::endl;
    }

    std::string shellRead(std::string shellOutput) {
        std::string data;
        FILE *stream;
        const int max_buffer = 256;
        char buffer[max_buffer];
        shellOutput.append(" 2>&1 ");

        stream=popen(shellOutput.c_str(), "r");
        if(stream){
            while(!feof(stream)){
                if(fgets(buffer, max_buffer, stream)!=NULL){
                    data.append(buffer);
                }
                pclose(stream);
            }
        }
        return data;
    }

    int determineIfYesOrNo(std::string userInput){
        //read input here
        //userInput matches yes, yea, yep, alright, positive, okay, sure, true, yeah, 1 - return yes.
        //else if matches no, nope, negative, nada, zero, zip, 0
        //else maybe output "I'm a robot sir, I don't understand approximations very well."
        
        std:string yesOrNo="";
        double affirmation;
        double sarcasmLevel=0;
        double positivity;
        double finalScore;

        if(userInput.find("N")!=std::string::npos||userInput.find("NO")!=std::string::npos||userInput.find("NA")!=std::string::npos||userInput.find("NEV")!=std::string::npos||userInput.find("DONT")!=std::string::npos||userInput.find("WRONG")!=std::string::npos||userInput.find("NEWP")!=std::string::npos||userInput.find(" AH ")!=std::string::npos||userInput.find("OI")!=std::string::npos||userInput.find("NUH ")!=std::string::npos||userInput.find("NEGATIVE")!=std::string::npos||userInput.find("x")!=std::string::npos||userInput.find("QUIT")!=std::string::npos||userInput.find("STOP")!=std::string::npos||userInput.find("0")!=std::string::npos||userInput.find("DO NOT ")!=std::string::npos){
            affirmation=0;
            yesOrNo="no";
        }else if(userInput.find("Y")!=std::string::npos||userInput.find("YE")!=std::string::npos||userInput.find("ALRIGHT")!=std::string::npos||userInput.find("OK")!=std::string::npos||userInput.find("SURE")!=std::string::npos||userInput.find("UNDERSTOOD")!=std::string::npos||userInput.find("I UNDERSTAND")!=std::string::npos||userInput.find("I GET IT")!=std::string::npos||userInput.find("GOT IT")!=std::string::npos||userInput.find("AIGHT")!=std::string::npos||userInput.find("RIGHT")!=std::string::npos||userInput.find("DO ")!=std::string::npos||userInput.find("RIGHT")!=std::string::npos||userInput.find("POSITIVE")!=std::string::npos||userInput.find("START")!=std::string::npos||userInput.find("1")!=std::string::npos){
            affirmation=1;
            yesOrNo="yes";
        }else{
            affirmation=0.5;
            yesOrNo="maybe";
        }
        return affirmation;
    }

    void digestResponse(){
        queryString outputString=findMatch(userInput);
        int nSelection=rand()%outputString.size();
        printOutput=outputString[nSelection];
        if(printOutput==prevOutput){
            outputString.erase(outputString.begin()+nSelection);
            nSelection=rand()%outputString.size();
            printOutput=outputString[nSelection];
        }
        std::cout<<printOutput<<std::endl;
    }

    void exit(){
        system("bash");
    }

};

class systemSounds{
public:
    void alert(){
        system("play -q -n synth 0.5 sin 6000 && play -q -n synth 0.5 sin 300 && play -q -n synth 0.5 sin 6000 && play -q -n synth 0.5 sin 300 && play -q -n synth 0.5 sin 6000 && play -q -n synth 0.5 sin 300");
    }

    void success(){
        system("play -q -n synth 0.015 sin 6000 && play -q -n synth 0.015 sin 300 && play -q -n synth 0.015 sin 6000 && play -q -n synth 0.015 sin 300 && play -q -n synth 0.015 sin 6000 && play -q -n synth 0.015 sin 300");
    }

    void abort(){
        system("play -q -n synth 0.09 sawtooth 500 && play -q -n synth 0.1 sawtooth 500 &&sleep 0.25&& play -q -n synth 0.09 sawtooth 500 && play -q -n synth 0.1 sawtooth 500 &&sleep 0.25&& play -q -n synth 0.09 sawtooth 500 && play -q -n synth 0.1 sawtooth 500");    
    }

    void failure(){
        system("play -q -n synth 0.2 sawtooth 200 && play -q -n synth 0.2 sawtooth 200");
    }

    void loading(){
        system("play -q -n synth 0.01 sawtooth 5000 && play -q -n synth 0.001 sin 300 && play -q -n synth 0.001 sin 400 && play -q -n synth 0.001 sin 450 && play -q -n synth 0.001 sin 500 && play -q -n synth 0.001 sin 550 && play -q -n synth 0.001 sin 600 && play -q -n synth 0.001 sin 650&& play -q -n synth 0.001 sin 750&& play -q -n synth 0.001 sin 950&& play -q -n synth 0.001 sin 1500&& play -q -n synth 0.001 sin 2500 && play -q -n synth 0.001 sin 3500 && play -q -n synth 0.001 sin 4500 && play -q -n synth 0.001 sin 5500 && play -q -n synth 0.001 sin 6550 && play -q -n synth 0.001 sin 9000 && play -q -n synth 0.001 sin 15000");
    }

    void notification(){
        system("play -q -n synth 0.05 sin 200 && play -q -n synth 0.05 sin 300");
    }

    void incomingCall(){
        system("play -q -n synth 0.1 sin 1000 && play -q -n synth 0.1 sin 1500 && play -q -n synth 0.1 sin 1000 && play -q -n synth 0.1 sin 1500 && sleep 0.65 && play -q -n synth 0.1 sin 1000 && play -q -n synth 0.1 sin 1500 && play -q -n synth 0.1 sin 1000 && play -q -n synth 0.1 sin 1500");
    }

    void disconnect(){
        system("play -q -n synth 0.05 sin 500 && play -q -n synth 0.05 sin 300");
    }

    void retry(){
        system("play -q -n synth 0.05 sin 500 && play -q -n synth 0.05 sin 500");
    }

    void alarmClock(){
        system("play -q -n synth 0.2 sin 4000 && play -q -n synth 0.2 sin 4000");
        system("play -q -n synth 0.2 sin 4000 && play -q -n synth 0.2 sin 4000");
        system("play -q -n synth 0.2 sin 4000 && play -q -n synth 0.2 sin 4000");
        system("play -q -n synth 0.2 sin 4000 && play -q -n synth 0.2 sin 4000");
        system("play -q -n synth 0.2 sin 4000 && play -q -n synth 0.2 sin 4000");
    }

    void restart(){
        system("clear");
        system("echo \"Restarting...\n■ \" && play -q -n synth 0.015 sin 880 || echo -e \"\a\"");
        system("sleep 0.75");
        system("clear");
        system("echo \"Restarting...\n■ . ■ \" && play -q -n synth 0.015 sin 880 || echo -e \"\a\"");
        system("sleep 0.75");
        system("clear");
        system("echo \"Restarting...\n■ . ■ . ■\" && play -q -n synth 0.015 sin 880 || echo -e \"\a\"");
        system("clear");
        system("exit");
        system("g++ chatbot.cpp -w && ./a.out");
    }

    void quindarIntro(){
        system("play -q -n synth 0.1 sin 2525");
    }

    void quindarOutro(){
        system("play -q -n synth 0.1 sin 2575");
    }
    void exit(){
        system("play -q -n synth 0.02 sin 1000 && play -q -n synth 0.02 sin 900");
    }
};

class deviceServices{
public:
    int alarmTime;
    time_t t=time(NULL);
    tm* timePtr=localtime(&t);
    int twelveHourHH=0;
    int twentyFourHourHH=(timePtr->tm_hour);
    int min=(timePtr->tm_min);
    std::string minuteLeadingZero="";
    int dayOfTheWeekNumber=(timePtr->tm_wday);
    int monthNumber=(timePtr->tm_mon+1);
    std::string meridian="";
    std::string day="";
    std::string dayPart="";
    std::string month="";
    int dayNumber=(timePtr->tm_mday);
    int year=(timePtr->tm_year)+1900;
   
    IOServices io;

    void batteryStats(){
        std::string batteryPercentage=io.shellRead("upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -oP '(percentage:).*(?<=)' | tr -d 'percentage: '");
        std::string batteryHoldingCapacity=io.shellRead("upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -oP '(capacity:).*(?<=)' | tr -d 'capacity: '");
        std::string voltage=io.shellRead("upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -oP '(voltage:).*(?<=)' | tr -d 'voltage: '");
        std::string currentEnergy=io.shellRead("upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -oP '(energy:).*(?<=)' | tr -d 'energy: '");
    }

    void memoryChecker(){
        io.shellRead("free");
    }

    void locationCoordinates(){
        io.shellRead("echo \"Latitude:\"  | tr '\n' ' ' && curl -s https://iplocation.com | grep -oP '(?<=<td class=\"lat\">).*(?=</td>)' | tr '\n' ' '");
        io.shellRead("echo \"Longitude:\"  | tr '\n' ' ' && curl -s https://iplocation.com | grep -oP '(?<=<td class=\"lng\">).*(?=</td>)' | tr '\n' ' '");
    }

    std::string currentLocationCity(){
        std::string city=io.shellRead("curl -s http://wttr.in/?format=%l | grep -oP '(?<=).*(,)' | tr -d '\n' | tr -d ','");
        return city;
    }

    std::string currentLocationSubdivision(){
        std::string subdivision=io.shellRead("curl -s https://iplocation.com | grep -oP '(?<=<span class=\"region_name\">).*(?=</span>)' | tr '\n' ' ' && echo \",\" | tr '\n' ' '");
        return subdivision;
    }

    std::string currentLocationCountry(){
        std::string country=io.shellRead("curl -s https://iplocation.com | grep -oP '(?<=<span class=\"country_name\">).*(?=</span>)' | tr '\n' ' '");
        return country;
    }

    int weatherTemperatureCelsius(){
        std::string temperatureString=io.shellRead("curl -s wttr.in/?format=%t | tr -d '+' | tr -d '\n' | tr -d '°' | tr -d 'C'");
        int temperature = atoi(temperatureString.c_str());
        return temperature;
    }

    std::string weatherCondition(){
        std::string condition=io.shellRead("curl -s wttr.in/?format=%C | grep -oP '(?<=).*(?<=)' | tr -d '\n'");
        return condition;
    }

    std::string weatherWindspeed(){
        std::string speed=io.shellRead("curl -s wttr.in/?format=%w | tr -d '\n' | tr -d '↑↓→←↗↖↘↙' | tr -d 'km/h'"); 
        return speed;
    }

    std::string weatherWindDirection(){
        std::string directionSymbol=io.shellRead("curl -s wttr.in/?format=%w | tr -d '\n' | tr -d ' ' | tr -d '0123456789' |tr -d 'km/h'");
        std::string direction;
        if(directionSymbol=="↑"){
            direction="north";
        }else if(directionSymbol=="↓"){
            direction="south";
        }else if(directionSymbol=="→"){
            direction="east";
        }else if(directionSymbol=="←"){
            direction="west";
        }else if(directionSymbol=="↗"){
            direction="northeast";
        }else if(directionSymbol=="↖"){
            direction="northwest";
        }else if(directionSymbol=="↘"){
            direction="southeast";
        }else if(directionSymbol=="↙"){
            direction="southwest";
        }
        return direction;
    }

    std::string weatherPrecip(){
        std::string precip=io.shellRead("curl -s wttr.in/?format=%p | tr -d '\n' | tr -d 'mm'");
        return precip;
    }

    std::string getIP(){
        std::string IP=io.shellRead("curl -s https://iplocation.com | grep -oP '(?<=<td><b class=\"ip\">).*(?=</b></td>)' | tr -d '\n'");
        int connectionStatus;
        if(IP==""){
            connectionStatus=0;
        }else{
            connectionStatus=1;
        }
        return IP;
    }

    int checkConnection(){
        int connectionStatus;
        if(getIP()==""){
            connectionStatus=0;
        }else{
            connectionStatus=1;
        }
        return connectionStatus;
    }

    std::string runInShell(){

    }

    int timeKeeperHH(){
        if(twentyFourHourHH==0){
            twelveHourHH=12;
        }else if(twentyFourHourHH==13){
            twelveHourHH=1;
        }else if(twentyFourHourHH==14){
            twelveHourHH=2;
        }else if(twentyFourHourHH==15){
            twelveHourHH=3;
        }else if(twentyFourHourHH==16){
            twelveHourHH=4;
        }else if(twentyFourHourHH==17){
            twelveHourHH=5;
        }else if(twentyFourHourHH==18){
            twelveHourHH=6;
        }else if(twentyFourHourHH==19){
            twelveHourHH=7;
        }else if(twentyFourHourHH==20){
            twelveHourHH=8;
        }else if(twentyFourHourHH==21){
            twelveHourHH=9;
        }else if(twentyFourHourHH==22){
            twelveHourHH=10;
        }else if(twentyFourHourHH==23){
            twelveHourHH=11;
        }else{
            twelveHourHH=twelveHourHH+twentyFourHourHH;
        }
        
        return twelveHourHH;
    }


    std::string minuteWithLeadingZero(){
        if(min==0){
            minuteLeadingZero="00";
        }else if(min==1){
            minuteLeadingZero="01";
        }else if(min==2){
            minuteLeadingZero="02";
        }else if(min==3){
            minuteLeadingZero="03";
        }else if(min==4){
            minuteLeadingZero="04";
        }else if(min==5){
            minuteLeadingZero="05";
        }else if(min==6){
            minuteLeadingZero="06";
        }else if(min==7){
            minuteLeadingZero="07";
        }else if(min==8){
            minuteLeadingZero="08";
        }else if(min==9){
            minuteLeadingZero="09";
        }else{
            minuteLeadingZero=std::to_string(min);
        }
        
        return minuteLeadingZero;
    }

    std::string partOfDay(){
        if(twentyFourHourHH>=0&&twentyFourHourHH<=5){
            dayPart="evening";
        }else if(twentyFourHourHH>=5&&twentyFourHourHH<=12){
            dayPart="morning";
        }else if(twentyFourHourHH>=12&&twentyFourHourHH<=17){
            dayPart="afternoon";
        }else if(twentyFourHourHH>=17&&twentyFourHourHH<=23){
            dayPart="evening";
        }
        return dayPart;
    }

    std::string meridianFinder(){
        if(twentyFourHourHH>=0&&twentyFourHourHH<=5){
            meridian="A.M.";
        }else if(twentyFourHourHH>=5&&twentyFourHourHH<=12){
            meridian="A.M.";
        }else if(twentyFourHourHH>=12&&twentyFourHourHH<=17){
            meridian="P.M.";
        }else if(twentyFourHourHH>=17&&twentyFourHourHH<=23){
            meridian="P.M.";
        }
        return meridian;
    }

    std::string dateKeeperMMString(){
        //switch() doesn't work for some reason.
        if(monthNumber==1){
            month="January";
        }else if(monthNumber==2){
            month="February";
        }else if(monthNumber==3){
            month="March";
        }else if(monthNumber==4){
            month="April";
        }else if(monthNumber==5){
            month="May";
        }else if(monthNumber==6){
            month="June";
        }else if(monthNumber==7){
            month="July";
        }else if(monthNumber==8){
            month="August";
        }else if(monthNumber==9){
            month="September";
        }else if(monthNumber==10){
            month="October";
        }else if(monthNumber==11){
            month="November";
        }else if(monthNumber==12){
            month="December";
        }
        return month;
    }   

    std::string dateKeeperDayOfTheWeek(){
        //switch() doesn't work for some reason.
        if(dayOfTheWeekNumber==1){
            day="Monday";
        }else if(dayOfTheWeekNumber==2){
            day="Tuesday";
        }else if(dayOfTheWeekNumber==3){
            day="Wednesday";
        }else if(dayOfTheWeekNumber==4){
            day="Thursday";
        }else if(dayOfTheWeekNumber==5){
            day="Friday";
        }else if(dayOfTheWeekNumber==6){
            day="Saturday";
        }else if(dayOfTheWeekNumber==7){
            day="Sunday";
        }
        return day;
    }

    void installJarvisToDevice(){
        system("chmod +x install.sh && ./install.sh");
    }

    void uninstallJarvisFromDevice(){
        system("./uninstall.sh");
    }

    void updateJarvis(){
        system("string=`git pull origin master`; if [[ $string == *\"Already up to date\"* ]]; then clear; echo \"I'm already on the latest version\"; else clear; echo \"Done updating. Restarting...\" && sleep 3; fi");
    }

    void IoTdaemon(){

    }
};

class uxServices{
public:
    deviceServices thisDevice;
    IOServices io;
    userData data;
    systemSounds sounds;
    void goodDaySequence(){
        deviceServices timing;
        std::cout<<"Good "+thisDevice.partOfDay()+".";
        std::cout<<" It's ";
        std::cout<<thisDevice.timeKeeperHH();
        std::cout<<":";
        std::cout<<thisDevice.minuteWithLeadingZero();
        std::cout<<" "+thisDevice.meridianFinder();
        std::cout<<" The weather in ";
        std::cout<<thisDevice.currentLocationCity();
        std::cout<<" is ";
        std::cout<<thisDevice.weatherTemperatureCelsius();
        std::cout<<" degrees celsius with ";
        std::cout<<thisDevice.weatherCondition();
        std::cout<<". The wind is currently ";
        std::cout<<thisDevice.weatherWindDirection();
        std::cout<<"erly at ";
        std::cout<<thisDevice.weatherWindspeed();
        std::cout<<"kilometers per hour, with a precipitation of ";
        std::cout<<thisDevice.weatherPrecip();
        std::cout<<" millimeters expected.";
        std::cout<<std::endl;
    }   

    void meteorologyBoard(){
        system("curl wttr.in");
    }

    void englishDictionary(){
        system("read -p \"What would you like me to define? \" query; curl -s dict://dict.org/d:$query:gcide");  //if input null echo I didnt quite get that sir
    }

    void DESTROYCOMPUTER(){
        std::cout<<"Are you sure about this, ";
        std::cout<<data.toBeCalled;
        std::cout<<"?"<<std::endl;
        std::cout<<"Computer system's knackered, ";
        std::cout<<data.toBeCalled;
        std::cout<<".";
        //system("");
    }

    void firstTimeGreet(){
        system("clear");
        system("echo -n \"Hello, I'm Jarvis. I run things here.\"");
        system("sleep 2 && clear");
        system("echo -n \"Can I know your FIRST name please?  \"; read FNAME; sed -i -e \"s/fname/$FNAME/g\" userdata.h");
        system("sleep 1 && clear");
        system("play -q -n synth 0.015 sin 880 || echo -e \"\a\" && clear");
        system("echo -n \"Can I know your LAST name please?  \"; read LNAME; sed -i -e \"s/lname/$LNAME/g\" userdata.h");
        system("sleep 1 && clear");
        system("play -q -n synth 0.015 sin 880 || echo -e \"\a\" && clear");
        system("echo -n \"Can I know your GENDER please (male/female)?  \"; read GENDER; if [[ \"$GENDER\" == *\"m\"* ]] || [[ \"$GENDER\" == *\"b\"* ]]; then sed -i -e \"s/pref/Sir/g\" userdata.h && sed -i -e \"s/title/Mr./g\" userdata.h && sed -i -e \"s/sex/male/g\" userdata.h; else sed -i -e \"s/pref/Ma'am/g\" userdata.h && sed -i -e \"s/title/Ms./g\" userdata.h && sed -i -e \"s/sex/male/g\" userdata.h; fi;");
        system("sleep 1 && clear");
        system("play -q -n synth 0.015 sin 880 || echo -e \"\a\" && clear");
        system("sed -i -e 's/int isDataSet=0;/int isDataSet=1;/g' userdata.h");
        system("sleep 1 && clear");
        system("echo -n \"Done. Restart program.\"");
        std::cout<<std::endl;
    }
    void socialMediaScraper(){
        system("read -p \"Who would you like to search for? \" query; scraperurl=\"https://www.social-searcher.com/search-users/?ntw=&q6=\"; finalurl=\"$scraperurl $query\"; xdg-open \"$finalurl\";"); //if input null echo I didnt quite get that sir
    }

    void deviceSearch(){
        system("read -p \"What would you like to search for? \" query; grep -iR --exclude-dir='/sys' --exclude-dir='/proc' --exclude-dir='/dev' \"$query\""); //if input null echo I didnt quite get that sir
    }

    void diagnosticsMode(){
        system("");
    }

    void flightMode(){

    }

    void weaponsMode(){

    }

};

int main(int argc, char *argv[]){
    srand((unsigned) time(NULL));
    std::cout.flush();
    system("clear");
    
    systemSounds sounds;
    IOServices io;
    uxServices ux;
    deviceServices thisDevice;
    userData data;
    //
    //
    //
    //Add warning if not connected to the internet saying limited capabilities.
    //
    //
    //
    sounds.loading();
    sounds.success();
    system("clear");
    while(1){
        if(data.isDataSet==1){
            std::cout<<" ▶ ";
            prevOutput=printOutput;
            prevInput=userInput;
            std::getline(std::cin, userInput);
            io.inputBuffer(userInput);
            queryString outputString=io.findMatch(userInput);

            io.determineIfYesOrNo(userInput);

            if(userInput.find("BYE")!=std::string::npos||userInput.find("SEE Y")!=std::string::npos||userInput.find("GROW A SPINE")!=std::string::npos||userInput.find("GET OUT")!=std::string::npos||userInput=="q"||userInput=="CYA"||userInput=="CIAO"||userInput=="QUITS"||userInput=="QUIT"||userInput=="EXIT"){
                io.digestResponse();
                sounds.exit();
                break;
            }else if(userInput.find("CLEAR")!=std::string::npos){
                system("clear");
            }else if(userInput.find("TIME")!=std::string::npos){
                std::cout<<"It's ";
                std::cout<<thisDevice.timeKeeperHH();
                std::cout<<":";
                std::cout<<thisDevice.minuteWithLeadingZero();
                std::cout<<" ";
                std::cout<<thisDevice.meridianFinder();
                std::cout<<", ";
                std::cout<<data.toBeCalled;
                std::cout<<"."<<std::endl;
            }else if(userInput.find("DATE")!=std::string::npos||userInput.find("DAY")!=std::string::npos&&userInput.find("IS")!=std::string::npos){
                std::cout<<"It's ";
                std::cout<<thisDevice.dateKeeperDayOfTheWeek();
                std::cout<<", ";
                std::cout<<thisDevice.dateKeeperMMString();
                std::cout<<" ";
                std::cout<<thisDevice.dayNumber;
                std::cout<<", ";
                std::cout<<thisDevice.year;
                std::cout<<", ";
                std::cout<<data.toBeCalled;
                std::cout<<"."<<std::endl;
            }else if(userInput.find("REBOOT")!=std::string::npos||userInput.find("RECOMPILE")!=std::string::npos||userInput.find("RESTART")!=std::string::npos){
                sounds.restart();
            }else if((userInput.find("WHERE")!=std::string::npos&&userInput.find("AM I")!=std::string::npos)||userInput.find("CITY")!=std::string::npos||userInput.find("PLACE")!=std::string::npos||userInput.find("LOCATION")!=std::string::npos||userInput.find("IM LOST")!=std::string::npos){
                std::cout<<"It appears you're in"<<std::endl;
                thisDevice.currentLocationCity();
                thisDevice.currentLocationSubdivision();
                thisDevice.currentLocationCountry();
                std::cout<<", ";
                std::cout<<data.toBeCalled;
                std::cout<<"."<<std::endl;
            }else if(userInput.find("COORDINATES")!=std::string::npos){
                std::cout<<"Your coordinates are";
                thisDevice.locationCoordinates();
                std::cout<<", ";
                std::cout<<data.toBeCalled;
                std::cout<<"."<<std::endl;
            }else if(userInput.find("METEO")!=std::string::npos||userInput.find("WEATHERBOARD")!=std::string::npos||userInput.find("WEATHERMAN")!=std::string::npos||userInput.find("WEATHER")!=std::string::npos&&userInput.find("IN DETAIL")!=std::string::npos||userInput.find("WEATHER")!=std::string::npos&&userInput.find("EXPERT")!=std::string::npos){
                ux.meteorologyBoard();
            }else if(userInput.find("WEATHER")!=std::string::npos||userInput.find("FORECAST")!=std::string::npos){
                std::cout<<"It is currently ";
                std::cout<<thisDevice.weatherTemperatureCelsius();
                std::cout<<" degrees celsius in ";
                std::cout<<thisDevice.currentLocationCity();
                std::cout<<"."<<std::endl;
            }else if(userInput.find("RUN")!=std::string::npos){
                thisDevice.runInShell();
            }else if(userInput.find("IP ADDRESS")!=std::string::npos||userInput.find("PUBLIC IP")!=std::string::npos||userInput.find("MY")!=std::string::npos&&userInput.find("IP")!=std::string::npos){
                std::cout<<thisDevice.getIP()<<std::endl;
            }else if(userInput.find("AM I")!=std::string::npos||userInput.find("ARE YOU")!=std::string::npos||userInput.find("CHECK")!=std::string::npos&&userInput.find("CONNECTION")||userInput.find("CONNECTED")!=std::string::npos&&userInput.find("INTERNET")!=std::string::npos||userInput.find("ONLINE")!=std::string::npos){
                if(thisDevice.checkConnection()==1){
                    std::cout<<"I am indeed connected to the Internet";
                    std::cout<<", ";
                    std::cout<<data.toBeCalled;
                    std::cout<<"."<<std::endl;
                }else if(thisDevice.checkConnection()==0){
                    std::cout<<"I cannot connect to the Internet. My capabilities offline are limited"<<std::endl;
                    std::cout<<", ";
                    std::cout<<data.toBeCalled;
                    std::cout<<"."<<std::endl;
                }
            }else if(userInput.find("GOOD")!=std::string::npos&&userInput.find("DAY")!=std::string::npos||userInput.find("AFTERNOON")!=std::string::npos||userInput.find("MORNING")!=std::string::npos||userInput.find("EVENING")!=std::string::npos){
                ux.goodDaySequence();
            }else if(userInput.find("ATC")!=std::string::npos||userInput.find("AIR TRAFFIC")!=std::string::npos||userInput.find("FLIGHT PATH")!=std::string::npos||userInput.find("FLIGHT RADAR")!=std::string::npos){
                system("xdg-open https://uk.flightaware.com/live/map#");
            }else if(userInput.find("MAP")!=std::string::npos){
                system("telnet mapscii.me");
            }else if(userInput.find("DEFINE")!=std::string::npos||userInput.find("MEANING")!=std::string::npos||userInput.find("DICTIONARY")!=std::string::npos){
                ux.englishDictionary();
            }else if(userInput.find("DESTROY")!=std::string::npos&&userInput.find("EVERYTHING")!=std::string::npos||userInput.find("COMPUTER")!=std::string::npos||userInput.find("PC")!=std::string::npos||userInput.find("FILES")!=std::string::npos){
                ux.DESTROYCOMPUTER();
            }else if(userInput.find("ASK")!=std::string::npos){
                ux.firstTimeGreet();
            }else if(userInput.find("INSTALL")!=std::string::npos||userInput.find("CHECK")!=std::string::npos||userInput.find("SYSTEM")!=std::string::npos&&userInput.find("UPDATE")!=std::string::npos||userInput.find("UPDATES")!=std::string::npos){
                thisDevice.updateJarvis();
            }else if(userInput.find("INSTALL")!=std::string::npos&&userInput.find("YOU")!=std::string::npos){
                thisDevice.installJarvisToDevice();
            }else if(userInput.find("UNINSTALL")!=std::string::npos&&userInput.find("YOU")!=std::string::npos){
                thisDevice.uninstallJarvisFromDevice();
            }else if(userInput.find("SEARCH")!=std::string::npos&&userInput.find("SOCIAL")!=std::string::npos||userInput.find("SCRAPE")&&userInput.find("SOCIAL")!=std::string::npos){
                ux.socialMediaScraper();
            }else if(userInput.find("SEARCH")!=std::string::npos&&userInput.find("FILES")||userInput.find("COMPUTER")!=std::string::npos||userInput.find("DEVICE")!=std::string::npos||userInput.find("SYSTEM")!=std::string::npos){
                ux.deviceSearch();
            }else if(outputString.size()==0){
                io.unknownInputHandler(userInput);
            }else{
                io.digestResponse();
            }
        }else{
            ux.firstTimeGreet();
            break;
        }
    }
    return 0;
}
