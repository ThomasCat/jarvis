#!/bin/bash 
if [ -d "//opt/owaisshaikh-jarvis/" ]; then 
	echo "I'm already installed on your system. Would you like to reinstall/repair me? ▶ " | tr -d "\n"; read CHOICE;
	if [[ "$CHOICE" == *"y"* ]]; then
		echo "Reinstalling...";
		sudo rm -rf //opt/owaisshaikh-jarvis/;
		sudo mkdir //opt/owaisshaikh-jarvis/;
		sudo chmod -R 755 //opt/owaisshaikh-jarvis/;
		sudo chown -R $USER //opt/owaisshaikh-jarvis/;
		cd //opt/owaisshaikh-jarvis/;
		git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/jarvis.git;
		cd jarvis;
		chmod +x install.sh; 
		chmod +x uninstall.sh;
		sudo cp "jarvis" //bin
		sudo chmod +x //bin/jarvis;
		clear;
		echo "Successfully repaired.";
		echo "Exit and type 'jarvis' to see if everything works.";
		exit; exit; exit;
	elif [[ "$CHOICE" == *"n"* ]] || [[ "$CHOICE" == *"e"* ]]; then
		exit;
	fi;
else
	clear;
	echo "Now installing...";
	sudo mkdir //opt/owaisshaikh-jarvis/;
	sudo chmod -R 755 //opt/owaisshaikh-jarvis/;
	sudo chown -R $USER //opt/owaisshaikh-jarvis/;
	cd //opt/owaisshaikh-jarvis/;
	git clone https://gitlab.com/5b43f91c61170e88de567951ebbec765/jarvis.git;
	cd jarvis;
	chmod +x install.sh; 
	chmod +x uninstall.sh;
	sudo cp "jarvis" //bin
	sudo chmod +x //bin/jarvis;
	clear;
	echo "Successfully installed.";
	echo "You can start me by typing 'jarvis' in the terminal at any time.";
	exit; exit;
	fi;

fi;